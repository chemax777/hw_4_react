import { Component } from "react";
import ProductCard from "../product-card/product-card";

import "./product-list.css"

class ProductsList extends Component {

    state = {
        products: []
    }

    componentDidMount() {
        const url = 'https://fakestoreapi.com/products'
        let responce = fetch(url)
        responce.then(data => data.json())
            .then(data => {
                this.setState({
                    products: data
                })
            })
    }

    render() {
        return (
            <div className="product-list">
                <ProductCard products={this.state.products}></ProductCard>
            </div>
        )
    }
}

export default ProductsList