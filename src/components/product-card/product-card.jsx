import "./product-card.css"

function ProductCard({ products }) {
    return (
        products.map(product => {
            const { id, category, image, title, description, price, rating } = product
            return (
                <div key={id} className="product-card">
                    <h3 className="product-card-category">Category: {category}</h3>
                    <div className="product-card-photo">
                        <img src={image} alt={title} />
                    </div>
                    <h2 className="product-card-title">{title}</h2>
                    <div className="product-card-description">{description}</div>
                    <div className="product-card-price">{price + "$"}</div>
                    <div className={rating.count > 0 ? "product-card-in-stock" : "product-card-out-of-stock"}>{rating.count > 0 ? "In stock" : "Out of stock"}</div>
                    <button className="add-to-card-btn">Add to cart</button>
                </div>
            )
        })
    )
}

export default ProductCard

